import { IModelDSLGenEngineContext } from '../../imodel-dslgen-engine-context';
import { AppUILogicWriter } from './app-uilogic-writer';

export class BuiltinAppUILogicWriterBase extends AppUILogicWriter {}
