module.exports = {
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  quoteProps: 'as-needed',
  arrowParens: 'avoid',
  tabWidth: 2,
  useTabs: false,
  endOfLine: 'lf',
};
