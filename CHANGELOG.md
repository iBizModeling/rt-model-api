# 版本变更日志

这个项目的所有关键变化都将记录在此文件中.

此日志格式基于 [Keep a Changelog](https://keepachangelog.com/zh-CN/1.0.0/),
并且此项目遵循 [Semantic Versioning](https://semver.org/lang/zh-CN/).

## [Unreleased]

## [0.2.11] - 2024-04-23

### Added

- 增加向导表单上一步启用脚本代码、下一步启用脚本代码、完成启用脚本代码

## [0.2.10] - 2024-04-23

### Fixed

- 修复视图消息关闭模式为0时，removeMode属性丢失问题

## [0.2.8] - 2024-03-28

### Added

- 工具栏项（界面行为类型）增加边框样式和按钮样式

## [0.2.7] - 2024-03-20

### Added

- 树节点增加支持行编辑仅提交变化值

## [0.2.6] - 2024-03-19

### Added

- 关系界面组成员增加启用判断数据访问标识和启用模式
- 实体表格、树节点增加支持行编辑仅提交变化值

## [0.2.5] - 2024-03-18

### Added

- 图表增加实体图表数据表格对象
- 日历项、地图项、树节点（实体）增加链接值应用实体属性


## [0.2.4] - 2024-03-11

### Added

- IAppDEUIAction 新增参数 deeditForm

## [0.2.3] - 2024-03-7

### Changed

- 补充应用级缺失参数

## [0.2.2] - 2024-02-27

### Added

- IAppDETreeGridView 新增参数 gridRowActiveMode、enableRowEdit、rowEditDefault
- IAppViewLogic 新增参数 appDEUIActionId
- 面板容器支持定义行为组展开模式，表单、面板支持按钮列表元素

## [0.2.1] - 2024-02-21

### Added

- 系统计数器、应用计数器新增参数 navigateContexts、navigateParams、uniqueTag
- 实体界面行为组新增参数 appDataEntityId
- 编辑表单新增参数 appCounterRefId
- 表单新增参数 counterId、counterMode、appCounterRefId

## [0.2.0] - 2024-02-20

### Changed

- package.json 中删除 type="module" 定义

## [0.1.48] - 2024-02-04

### Added

- ApplicationWriter 新增 bottomInfo、headerInfo、title、caption、subCaption

## [0.1.47] - 2024-02-04

### Added

- AppUILogicRefViewWriter 新增 openMode
- AppDEEditViewWriter 新增 markOpenDataMode、enableDirtyChecking

## [0.1.46] - 2024-01-29

### Added

- ModelObjectWriter 补充 modelId、modelType 转换，满足开发态下跳转配置界面参数需求

## [0.1.45] - 2024-01-25

### Added

- 新增文件上传 osscat 相关

## [0.1.44] - 2024-01-24

### Added

- DEUIActionWriter 补充 asyncAction 转换

## [0.1.43] - 2024-01-23

### Added

- SysSearchBarWriter 新增 groupMode 模式

## [0.1.42] - 2024-01-18

### Added

- 补充代码表转换相关内容

## [0.1.41] - 2024-01-18

### Added

- AppDataEntityWriter 输出 dynaSysMode
- SysSearchBarGroupWriter 默认分组 defaultGroup
- DETreeNodeWriterBase 动态样式 dynaClass

## [0.1.39] - 2024-01-05

### Added

- 关系补充转换，嵌套数据以及限制删除相关

## [0.1.38] - 2024-01-04

### Added

- 表格树补充相关表格列模型的转换

## [0.1.37] - 2023-12-28

### Added

- DEACModeWriter 新增 deuiactionGroup、itemLayoutPanel 转换

## [0.1.36] - 2023-12-25

### Added

- DETreeNodeDataItemWriter 新增 getPSDETreeColumn 转换

## [0.1.35] - 2023-12-20

### Added

- AppDEMethodDTOFieldWriter 新增 refPickupAppDEFieldId 转换
- ApplicationWriter、AppViewWriter、dynaSysMode 新增 dynaSysMode 转换

## [0.1.34] - 2023-12-11

### Added

- 应用实体新增联合主键转换 unionKeyValueAppDEFieldIds

## [0.1.33] - 2023-12-07

### Fixed

- 修正 app.res.AppSubViewTypeRef 转换异常

## [0.1.32] - 2023-12-06

### Added

- 应用补充 appSubViewTypeRefs 模型转换
- appSubViewTypeRefs 修改其所属应用域

## [0.1.30] - 2023-12-04

### Added

- 表格列补充 resetItemNames 转换支持

## [0.1.25] - 2023-11-30

### Added

- 实体打印和报表支持插件 sysPFPluginId
- 应用补充 appSubViewTypeRefs

## [0.1.23] - 2023-11-06

### Added

- 所有模型默认转 userTag、userTa2、userTag3、userTag4

## [0.1.22] - 2023-10-31

### Added

- 日历项、地图项支持自定义查询条件

## [0.1.21] - 2023-10-10

### Added

- markdown、text-area 支持自填模式、导航参数相关
- 实体逻辑新增 DECISION 类型
- 界面逻辑新增 DECISION 类型

## [0.1.20] - 2023-09-25

### Fixed

- AppViewMsgListWriter dynamicMode 判断改为数值匹配发布的模型

## [0.1.19] - 2023-09-21

### Changed

- showBusyIndicator 值为 false 的时候输出

## [0.1.18] - 2023-09-20

### Added

- AppPortletWriter 补充部件输出

## [0.1.17] - 2023-09-18

### Added

- HiddenDEGridEditItemWriter 补充输出
- rawItem 补充输出 rawContent、htmlContent、sysImage
- 代码编辑器新增 enableFullScreen、enableMinimap 配置转换

## [0.1.14] - 2023-09-11

### Changed

- maskMode 在 0 以上时都不忽略输出

## [0.1.13] - 2023-09-11

### Added

- 表单关系界面新增 maskInfo、maskMode 配置

## [0.1.12] - 2023-09-11

### Changed

- timeout 在 0 以上时都不忽略输出

## [0.1.11] - 2023-08-24

### Added

- 新增应用多语言转换

## [0.1.10] - 2023-08-23

### Added

- 部件支持布局面板
- 面板项容器支持脚本
- 表单和多数据部件支持 activeDataMode 模式

### Fixed

- 前端应用插件 id 转换处理补充

## [0.1.7] - 2023-08-03

### Changed

- 特殊默认值忽略机制补充，避免一些特殊默认值被过滤导致模型异常

## [0.1.6] - 2023-07-17

### Changed

- 优化默认引擎输出参数，删除未使用变量

## [0.1.5] - 2023-07-14

### Added

- 部件支持面板

## [0.1.4] - 2023-07-13

### Added

- portlet 具象类型数据模型转换实现

## [0.1.3] - 2023-07-10

### Added

- 补充遗漏的 navigateContexts、navigateParams 输出

## [0.1.1] - 2023-06-19

### Added

- 移动端所有视图补充转换 enablePullDownRefresh 参数

## [0.1.0] - 2023-06-16

### Added

- 多数据部件视图

## [0.0.1] - 2023-06-15

### Added

- 布局容器输出 predefinedType 预定义类型

## [0.0.1-beta.16] - 2023-06-08

### Added

- 补充基础输出 userParam

## [0.0.1-beta.15] - 2023-06-07

### Fixed

- 向导表单 stepActions 修正为和 json 模型匹配提取

## [0.0.1-beta.14] - 2023-05-31

### Fixed

- 修正忽略输出值，避免 flex 布局不生效

## [0.0.1-beta.13] - 2023-05-16

### Added

- 界面行为组项，补充输出图片、标题等

## [0.0.1-beta.12] - 2023-05-16

### Added

- 表单关系界面引用视图标识输出

## [0.0.1-beta.11] - 2023-05-10

### Added

- flex 布局新增 basis、shrink 输出
